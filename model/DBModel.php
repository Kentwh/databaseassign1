<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=testdb;charset=utf8mb4', 'root', '');
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = Array();
        $sqlGetBooks = $this->db->query("SELECT * FROM `Books` ORDER BY id ASC"); // Setup a query to get all books ordered by id
        while($row = $sqlGetBooks->fetch(PDO::FETCH_ASSOC))                     // Loop while you get something from fetch
        {
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']); // Add a new "Book" object with rows attribs to booklist
        }
        return $booklist;   // Return booklist

        // COULD ALSO BE DONE LIKE THIS:
        /*foreach($this->db->query('SELECT * FROM Books ORDER BY id') as $row)
        {
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }*/
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        $book = NULL;
        // Check if $id constains only numeric values, if not, return saying something went wrong
        if (!is_numeric($id) || $id <= 0) { return null; }
        // Prepare statment to get all book with given id
        $sqlGetBookById = $this->db->prepare("SELECT * FROM Books WHERE id=:ID");
        $sqlGetBookById->bindValue(':ID', $id, PDO::PARAM_INT);                 // Bind id

        $sqlGetBookById->execute();                                             // Execute sql statement

		$fetchedBook = $sqlGetBookById->fetch(PDO::FETCH_ASSOC);                // Fetch first book in the result
        // Transfer the results into a new book
        $book = new Book($fetchedBook['title'], $fetchedBook['author'], $fetchedBook['description'], $fetchedBook['id']);
        // Test to see if result is a valid result, if not, throw Exception and return NULL
        if ($book->title == NULL || $book->author == NULL || $book->id == NULL) { throw new Exception(); return null; }
        return $book;
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        // Is title or author is empty, abort!
        if ($book->title === "" || $book->author === "") { throw new Exception(); return; }
        if ($book->description == "") $book->description = NULL;

        // Prepare statement to insert a book
        $sqlAddBook = $this->db->prepare("INSERT INTO Books (title, author, description) VALUES (:title, :author, :description)");
        $sqlAddBook->bindValue(":title", $book->title, PDO::PARAM_STR);                 // Bind title
        $sqlAddBook->bindValue(":author", $book->author, PDO::PARAM_STR);               // Bind author
        $sqlAddBook->bindValue(":description", $book->description, PDO::PARAM_STR);     // Bind description
        $sqlAddBook->execute();                                                     // Execute sql statement
        $book->id = $this->db->lastInsertId();                                          // Get the newly added book's id from database
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        // Is title or author is empty or id is non-numeric, abort!
        if ($book->title === "" || $book->author === "" || !is_numeric($book->id) || $book->id <= 0) { throw new Exception(); return; }

        // Prepare statement to update a book
        $sqlModBook = $this->db->prepare("UPDATE Books SET title=:title,author=:author,description=:description WHERE id=:id");
        $sqlModBook->bindValue(":title", $book->title, PDO::PARAM_STR);             // Bind title
        $sqlModBook->bindValue(":author", $book->author, PDO::PARAM_STR);           // Bind author
        $sqlModBook->bindValue(":description", $book->description, PDO::PARAM_STR); // Bind description
        $sqlModBook->bindValue(":id", $book->id, PDO::PARAM_INT);                   // Bind id
        $sqlModBook->execute();                                                 // Execute sql statement
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        // If $id doesn't contain all numeric values, then return that something went wrong
        if (!is_numeric($id) || $id <= 0) { throw new Exception(); return; }
        // Prepare statment to delete book with given id
        $sqlDelBook = $this->db->prepare("DELETE FROM Books WHERE id=:id");
        $sqlDelBook->bindValue(":id", $id, PDO::PARAM_INT);                     // Bind id
        $sqlDelBook->execute();                                                 // execute statement
    }

}

?>
